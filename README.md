# Contributors #

### Everything ###
* Our amazing tutors

### Kosta, Michael, Jamie ###
* The mystical wizardry we humbly call "the template engine"
* Making Aaron's life easier

### How do I get set up? ###

* Download/install Python 3.x
* Open run.py in IDLE
* Run module
* Visit http://localhost:8888/

### Who do I talk to? ###

* If you would like your name to be added in contributors, contact Aaron via Facebook